import Vue from 'vue'
import App from './App.vue'

// 关闭开发提示
Vue.config.productionTip = false

// 第一种写法，vue3只有这种写法，用的多
new Vue({
  render: h => h(App),
}).$mount('#app')


//第二种写法，
// new Vue({
//   el:'#app',
//   render: h=> h(App)
// })