const state = {
    todos: [
        { id: 1, todoname: "111", isDone: false },
        { id: 2, todoname: "222", isDone: false },
        { id: 3, todoname: "333", isDone: false },
    ]
}
const actions = {}
const getters = {
    ISDONE(state) {
        return state.todos.filter(item => item.isDone).length
    },
    ALLTODOS(state) {
        return state.todos.length
    }
}
const mutations = {
    ADDTODO({ todos }, todoname) {
        const todo = { id: Date.now(), todoname, isDone: false }
        todos.unshift(todo)
    },
    UPDATAISDONE({ todos }, id) {
        const todo = todos.find(item => item.id === id)
        todo.isDone = !todo.isDone
    },
    DELTODO(state, id) {
        state.todos = state.todos.filter(item => item.id !== id)
    },

    DELISDONETODO(state) {
        state.todos = state.todos.filter(item => !item.isDone)
    },
    UPDATEALLISDONE(state, isDone) {
        state.todos.forEach(item => item.isDone = isDone)
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
}