import Vue from 'vue'
import Vuex from 'vuex'
import modules from './modules'

// 使用Vuex插件
Vue.use(Vuex)

export default new Vuex.Store({
    modules,

})
