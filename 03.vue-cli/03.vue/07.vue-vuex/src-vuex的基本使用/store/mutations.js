// 定义mutations函数
export default {
    INCREMENT(state) {
        state.count++
    },
    DECREMENT(state) {
        state.count--
    },
    INCREMENTASYNC(state) {
        setTimeout(() => {
            state.count++
        }, 1000);
    }
}