import Vue from 'vue'
import Vuex from 'vuex'
import state from './state'
import actions from './actions'
import mutations from './mutations'

Vue.use(Vuex)

export default new Vuex.Store({
    // 管理数据的对象
    state,
    // 包含n个间接管理数据的对象
    actions,
    // 包含n个用来直接更新数据的函数对象
    mutations,

})