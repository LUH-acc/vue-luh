
import axios from 'axios'

const state = {
    users: [],
    isSearch: true,
    isLoading: false,
}
const actions = {
    async search({ commit }, searchName) {
        console.log('searchName', searchName);
        commit('CHANGE_STATUS', true)
        const res = await axios({
            method: "GET",
            url: `https://api.github.com/search/users?q=${searchName}`,
        });
        console.log(res);
        commit('CHANGE_STATUS', false)
        commit('SEARCH', res.data.items)
    }
}
const getters = {}
const mutations = {
    SEARCH(state, users) {
        state.users = users
    },
    CHANGE_STATUS(state, flag) {
        state.isSearch = false,
            state.isLoading = flag
    }
}

export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations,
}