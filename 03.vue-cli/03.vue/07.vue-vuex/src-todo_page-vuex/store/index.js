import Vue from 'vue'
import Vuex from 'vuex'

// 使用Vuex插件
Vue.use(Vuex)

export default new Vuex.Store({
    // 定义state，存储信息
    state: {
        todos: [
            { id: 1, todoname: "111", isDone: false },
            { id: 2, todoname: "222", isDone: false },
            { id: 3, todoname: "333", isDone: false },
        ],
    },
    // 
    actions: {
        delTodo(store, id) {
            store.commit('DELTODO', id)
        }
    },
    // 定义修改状态的函数
    mutations: {
        ADDTODO({ todos }, todoname) {
            const todo = { id: Date.now(), todoname, isDone: false }
            todos.unshift(todo)
        },
        UPDATAISDONE({ todos }, id) {

            // console.log(todos === this.state.todos);
            const todo = todos.find(item => item.id === id)
            todo.isDone = !todo.isDone
        },
        DELTODO(state, id) {
            // ???
            state.todos = state.todos.filter(item => item.id !== id)
        },
        // DELTODO({todos}, id) {
        //     // ???
        //     todos = todos.filter(item => item.id !== id)
        // },

        DELISDONETODO(state) {
            // ???
            state.todos = state.todos.filter(item => !item.isDone)
        },
        UPDATEALLISDONE(state, isDone) {
            state.todos.forEach(item => item.isDone = isDone)
        }
    }

})
