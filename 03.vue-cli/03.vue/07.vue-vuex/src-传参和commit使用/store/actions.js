export default {
    // 可以接收两个参数
    increment(store, data) {
        // 1.发送请求，获取数据
        // 2.执行mutations函数，修改数据
        console.log('actions', data);
        store.commit('INCREMENT')
    },
    decrement({ commit }) {
        commit('DECREMENT')
    },
    incrementIfOdd({ commit, state }) {
        if ((state.count & 1) === 1) commit('INCREMENT')
    },
    incrementAsync({ commit }) {
        commit('INCREMENTASYNC')
    }
}