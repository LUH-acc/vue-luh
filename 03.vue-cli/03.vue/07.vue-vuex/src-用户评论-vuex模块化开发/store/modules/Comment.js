// 创建vuex代码
const state = {
    list: [
        { id: 0, name: "A", content: "A hello" },
        { id: 1, name: "B", content: "B hello" },
        { id: 2, name: "C", content: "C hello" },
    ],
}
const getters = {}
const actions = {}
const mutations = {
    ADD(state, { name, content }) {
        state.list.push({
            id: Math.random(),
            name,
            content,
        });
    },
    DEL(state, id) {
        state.list = state.list.filter((item) => {
            return item.id !== id;
        });
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}