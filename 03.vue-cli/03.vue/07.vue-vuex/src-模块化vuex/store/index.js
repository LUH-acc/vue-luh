import Vue from 'vue'
import Vuex from 'vuex'
import modules from './modules'

Vue.use(Vuex)

export default new Vuex.Store({
    /*   // 管理数据的对象
      state: {
          count: 0,
      },
      getters: {
          isOdd(state) {
              return (state.count & 1) === 1 ? "奇数" : "偶数";
          }
      },
      // 包含n个间接管理数据的对象
      actions: {
          // 可以接收两个参数
          increment(store) {
              // 1.发送请求，获取数据
              // 2.执行mutations函数，修改数据
  
              store.commit('INCREMENT')
          },
          decrement({ commit }) {
              commit('DECREMENT')
          },
          incrementIfOdd({ commit, state }, val) {
              if ((state.count & 1) === 1) commit('INCREMENT', val)
          },
          incrementAsync({ commit }, val) {
              setTimeout(() => {
                  commit('INCREMENT', val)
              }, 1000);
          }
      },
      // 包含n个用来直接更新数据的函数对象
      mutations: {
          INCREMENT(state, val) {
              state.count += val
          },
          DECREMENT(state, val) {
              state.count -= val
          },
  
      } */
    modules,
})