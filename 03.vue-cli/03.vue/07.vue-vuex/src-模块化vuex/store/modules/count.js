const state = {
    count: 0
}
const actions = {
    increment(store) {
        // 1.发送请求，获取数据
        // 2.执行mutations函数，修改数据

        store.commit('INCREMENT')
    },
    decrement({ commit }) {
        commit('DECREMENT')
    },
    incrementIfOdd({ commit, state }, val) {
        if ((state.count & 1) === 1) commit('INCREMENT', val)
    },
    incrementAsync({ commit }, val) {
        setTimeout(() => {
            commit('INCREMENT', val)
        }, 1000);
    }
}
const getters = {
    isOdd(state) {
        return (state.count & 1) === 1 ? "奇数" : "偶数";
    }
}
const mutations = {
    INCREMENT(state, val) {
        state.count += val
    },
    DECREMENT(state, val) {
        state.count -= val
    },
}

export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations
}