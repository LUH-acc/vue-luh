import count from './count'
import person from './person'

export default {
    count,
    person
}