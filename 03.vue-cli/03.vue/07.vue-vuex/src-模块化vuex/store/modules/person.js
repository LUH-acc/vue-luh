const state = {
    name: 'jack',
    age: 18
}
const getters = {}
const mutations = {}
const actions = {}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
}