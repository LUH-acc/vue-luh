import Vue from 'vue'
import router from './Route/route'
import App from './App'

Vue.config.productionTip = false

new Vue({
    // 这个对象是配置对象
    // 配置对象里面的键的名字不能修改
    router,
    render: h => h(App)
}).$mount('#app')