
import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeComp from '../Views/HomeComp'
import AboutComp from '../Views/AboutComp'
import NewsComp from '../Views/HomeComp/components/NewsComp'
import MessageComp from '../Views/HomeComp/components/MessageComp'
import DetailComp from '../Views/HomeComp/components/MessageComp/components/Detail'

Vue.use(VueRouter)

const route = new VueRouter({
    routes: [
        {
            name: 'HomeComp',//命名路由
            // 路由路径
            path: '/home',
            // 路由组件
            component: HomeComp,
            // 子路由
            children: [
                {
                    name: 'NewsComp',//命名路由
                    path: '/home/news',
                    component: NewsComp,
                    meta: {
                        title: '新闻'
                    }
                },
                {
                    name: 'MessageComp',
                    path: 'message',
                    component: MessageComp,
                    children: [
                        {
                            name: 'DetailComp',
                            // :id就是路由的params参数
                            path: 'detail/:id',
                            component: DetailComp,
                            meta: {
                                title: '信息'
                            },
                            props($route) {
                                // 返回值对象，会以props方式传递到组件内
                                return {
                                    ...$route.query,
                                    ...$route.params,
                                };
                            },
                        }
                    ]
                }
            ]
        },
        {
            path: '/about',
            component: AboutComp,
            meta: {
                title: '关于'
            }
        },
        {
            // 匹配任意地址，重定向到home组件
            path: '*',
            redirect: '/home'
        }
    ]
})

export default route