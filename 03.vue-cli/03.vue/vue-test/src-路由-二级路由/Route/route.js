
import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeComp from '../Views/HomeComp'
import AboutComp from '../Views/AboutComp'
import NewsComp from '../Views/HomeComp/components/NewsComp'
import MessageComp from '../Views/HomeComp/components/MessageComp'

Vue.use(VueRouter)

const route = new VueRouter({
    routes: [
        {
            // 路由路径
            path: '/home',
            component: HomeComp,
            children: [
                {
                    path: '/home/news',
                    component: NewsComp
                },
                {
                    path: 'message',
                    component: MessageComp
                }
            ]
        },
        {
            path: '/about',
            component: AboutComp
        },
        {
            path: '*',
            redirect: '/home'
        }
    ]
})

export default route