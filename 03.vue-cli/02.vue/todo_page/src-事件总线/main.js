import Vue from 'vue'
import App from './App'

new Vue({
    beforeCreate() {
        Vue.prototype.$bus = this
    },
    render: h => h(App)
}).$mount('#app')