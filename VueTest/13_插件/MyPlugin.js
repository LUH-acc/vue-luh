/* 
Vue的插件定义有两种方式

1. 函数形式

2.对象形式

*/
/* function MyPlugin(Vue) {


    // 绑定在Vue实例上面
    Vue.prototype.$say = function (word) {
        console.log('hello', word);
    }
    // 绑定在Vue上面
    Vue.say = function (word) {
        console.log('hello', word);
    }

    Vue.directive('upper-case', function (el, binding) {
        el.innerText = binding.value.toUpperCase()
    })
} */

const MyPlugin = {
    install: function (Vue) {
        // 绑定在Vue实例上面
        Vue.prototype.$say = function (word) {
            console.log('hello', word);
        }
        // 绑定在Vue上面
        Vue.say = function (word) {
            console.log('hello', word);
        }

        Vue.directive('upper-case', function (el, binding) {
            el.innerText = binding.value.toUpperCase()
        })
    }
}