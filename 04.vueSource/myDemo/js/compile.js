function Compile(el, vm) {
	this.$vm = vm;
	// 判断el是否是元素节点，是返回元素节点，不是查询
	this.$el = this.isElementNode(el) ? el : document.querySelector(el);

	if (this.$el) {
		// 文档碎片节点
		this.$fragment = this.node2Fragment(this.$el);
		//
		this.init();
		this.$el.appendChild(this.$fragment);
	}
}

Compile.prototype = {
	constructor: Compile,
	node2Fragment: function (el) {
		// 创建文档碎片节点，
		var fragment = document.createDocumentFragment(),
			child;

		// 通过循环，将每一个原生节点拷贝到fragment中，
		// 作用，fragment在内存中就行操作，减少了操作DOM元素，减少重排重绘，提升性能
		while ((child = el.firstChild)) {
			fragment.appendChild(child);
		}
		// 返回得到全部DOM元素的文档碎片节点
		return fragment;
	},

	init: function () {
		this.compileElement(this.$fragment);
	},

	compileElement: function (el) {
		// 返回所有的子节点，得到一个类数组对象
		var childNodes = el.childNodes,
			me = this;
		// 将类数组对象转换成数组，遍历数组元素，对每一个节点进行判断
		[].slice.call(childNodes).forEach(function (node) {
			var text = node.textContent;
			var reg = /\{\{(.*)\}\}/;

			// 节点是元素节点
			if (me.isElementNode(node)) {
				me.compile(node);
				// 节点是文本节点
			} else if (me.isTextNode(node) && reg.test(text)) {
				me.compileText(node, RegExp.$1.trim());
			}
			// 判断是否节点内是否有子节点
			if (node.childNodes && node.childNodes.length) {
				// 递归调用
				me.compileElement(node);
			}
		});
	},

	compile: function (node) {
		// 节点是元素节点,获取元素节点所有的属性
		var nodeAttrs = node.attributes,
			me = this;

		[].slice.call(nodeAttrs).forEach(function (attr) {
			var attrName = attr.name;
			// 判断属性名是否是指令属性
			if (me.isDirective(attrName)) {
				var exp = attr.value;
				var dir = attrName.substring(2);
				// 事件指令
				if (me.isEventDirective(dir)) {
					compileUtil.eventHandler(node, me.$vm, exp, dir);
					// 普通指令
				} else {
					compileUtil[dir] && compileUtil[dir](node, me.$vm, exp);
				}

				node.removeAttribute(attrName);
			}
		});
	},

	compileText: function (node, exp) {
		compileUtil.text(node, this.$vm, exp);
	},

	isDirective: function (attr) {
		return attr.indexOf("v-") == 0;
	},

	isEventDirective: function (dir) {
		return dir.indexOf("on") === 0;
	},

	isElementNode: function (node) {
		return node.nodeType == 1;
	},

	isTextNode: function (node) {
		return node.nodeType == 3;
	},
};

// 指令处理集合
var compileUtil = {
	text: function (node, vm, exp) {
		this.bind(node, vm, exp, "text");
	},

	html: function (node, vm, exp) {
		this.bind(node, vm, exp, "html");
	},

	model: function (node, vm, exp) {
		this.bind(node, vm, exp, "model");

		var me = this,
			val = this._getVMVal(vm, exp);
		node.addEventListener("input", function (e) {
			var newValue = e.target.value;
			if (val === newValue) {
				return;
			}

			me._setVMVal(vm, exp, newValue);
			val = newValue;
		});
	},

	class: function (node, vm, exp) {
		this.bind(node, vm, exp, "class");
	},

	bind: function (node, vm, exp, dir) {
		var updaterFn = updater[dir + "Updater"];

		updaterFn && updaterFn(node, this._getVMVal(vm, exp));
		// 产生闭包，function()
		new Watcher(vm, exp, function (value, oldValue) {
			updaterFn && updaterFn(node, value, oldValue);
		});
	},

	// 事件处理
	eventHandler: function (node, vm, exp, dir) {
		var eventType = dir.split(":")[1],
			fn = vm.$options.methods && vm.$options.methods[exp];

		if (eventType && fn) {
			node.addEventListener(eventType, fn.bind(vm), false);
		}
	},

	_getVMVal: function (vm, exp) {
		var val = vm;
		exp = exp.split(".");
		exp.forEach(function (k) {
			val = val[k];
		});
		return val;
	},

	_setVMVal: function (vm, exp, value) {
		var val = vm;
		exp = exp.split(".");
		exp.forEach(function (k, i) {
			// 非最后一个key，更新val的值
			if (i < exp.length - 1) {
				val = val[k];
			} else {
				val[k] = value;
			}
		});
	},
};

var updater = {
	textUpdater: function (node, value) {
		node.textContent = typeof value == "undefined" ? "" : value;
	},

	htmlUpdater: function (node, value) {
		node.innerHTML = typeof value == "undefined" ? "" : value;
	},

	classUpdater: function (node, value, oldValue) {
		var className = node.className;
		className = className.replace(oldValue, "").replace(/\s$/, "");

		var space = className && String(value) ? " " : "";

		node.className = className + space + value;
	},

	modelUpdater: function (node, value, oldValue) {
		node.value = typeof value == "undefined" ? "" : value;
	},
};
