function Observer(data) {
    // MVVM实例中的data
    // 这个this指向Observer的实例对象
    this.data = data;
    this.walk(data);
}

Observer.prototype = {
    constructor: Observer,
    walk: function (data) {
        var me = this;
        Object.keys(data).forEach(function (key) {
            // key为MVVM的data中的key,
            // convert -> 转换
            me.convert(key, data[key]);
        });
    },
    convert: function (key, val) {
        // defineReactive -> 定义激活
        // this.data === data
        this.defineReactive(this.data, key, val);
    },

    defineReactive: function (data, key, val) {
        // uid = 0 , subs = []
        var dep = new Dep();
        var childObj = observe(val);

        Object.defineProperty(data, key, {
            enumerable: true, // 可枚举
            configurable: false, // 不能再define
            get: function () {
                if (Dep.target) {
                    dep.depend();
                }
                return val;
            },
            set: function (newVal) {
                if (newVal === val) {
                    return;
                }
                val = newVal;
                // 新的值是object的话，进行监听
                childObj = observe(newVal);
                // 通知订阅者
                dep.notify();
            }
        });
    }
};

function observe(value, vm) {

    // 是否有数据
    if (!value || typeof value !== 'object') {
        return;
    }

    return new Observer(value);
};


var uid = 0;

function Dep() {
    // this === Dep实例
    this.id = uid++;
    this.subs = [];
}

Dep.prototype = {
    addSub: function (sub) {
        this.subs.push(sub);
    },

    depend: function () {
        Dep.target.addDep(this);
    },

    removeSub: function (sub) {
        var index = this.subs.indexOf(sub);
        if (index != -1) {
            this.subs.splice(index, 1);
        }
    },

    notify: function () {
        this.subs.forEach(function (sub) {
            sub.update();
        });
    }
};

Dep.target = null;